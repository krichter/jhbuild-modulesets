# jhbuild-modulesets for fast bootstrapping
## Initialization
Steps to do:

  * get `jhbuild` (tarball from https://developer.gnome.org/jhbuild/ or with `git` from https://github.com/GNOME/jhbuild)
  * build `jhbuild` with `./autogen.sh && make && make install` and add `$HOME/.local/bin/jhbuild` to your `PATH` environment variable

Now you can build with

```
jhbuild --file=$HOME/jhbuild-modulesets/.jhbuildrc --moduleset=$HOME/jhbuild-modulesets/jhbuild-modules.modules build --nodeps [module name]
```
